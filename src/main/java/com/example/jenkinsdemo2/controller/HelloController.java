package com.example.jenkinsdemo2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class HelloController {

    @GetMapping("hello")
    public String hello() {
        return "hello jenkins by aws ECR :`)";
    }

    @GetMapping("para/{para}")
    public String hello(@PathVariable("para") String para) {
        return "hello " + para + " :`)";
    }

    @GetMapping("ecr")
    public String ecr() {
        return "success access from ECR";
    }

}
